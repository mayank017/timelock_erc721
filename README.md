The smart contract contains the method to lock/unlock the token via help of the tokenId.
The locked token can not be transfered.Only trasfer will occur when token is in unlocked state.
Only the token Owner is allowed to change the locking status.

Newly Added Functions : 

removelock : unlock the token

addlock : Lock the token 

getNFTStatus : Get status of token.


futureTimelock : modifier to check status
